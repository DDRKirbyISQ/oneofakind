One of a Kind v1.02
by DDRKirby(ISQ)

==============
=Installation=
==============
Windows:
Extract from the zip file, then run OneOfAKind.exe.
If this doesn't work, you may wish to try OneOfAKind_VS.exe, which was compiled with Visual Studio instead of MonoDevelop.  However, you will need to have the .NET Framework installed.

Linux:
Extract from the tar.gz file, then run OneOfAKind.
You will need the following packages:
mono-runtime
gtk-sharp2
libsdl1.2debian-all
libsdl-mixer1.2
libsdl-ttf-2.0-0
libsdl-image1.2
Note: OneOfAKind has been reported to be incompatible with 64-bit linux systems.

Mac OS X:
Open the dmg file, then run OneOfAKind.app.

==============
=Known Issues=
==============
There probably are a bunch, and the Linux/OSX versions are most likely either wonky or broken due to time constraints.  Maybe in the next update...

==========
=Controls=
==========
Arrow Keys, Space, and Escape (as explained in-game).  You can also use Enter instead of Space, or R instead of Escape.
To quit, simply close the application window.

===========
=Changelog=
===========
*1.02 - *
-Fixed a bug that caused the game to crash if you backtracked from the final room.

*1.01 - 12/18/11*
-Fixed some minor missing pixel shading on screen 3.
-Fixed a bug that caused the game to crash right at the ending.

*v1.00 - 12/18/11*
First release!
This version was submitted for Ludum Dare 22.

=================================================================================
More info at http://sites.google.com/site/ddrkirbyisqcodingprojects/one-of-a-kind