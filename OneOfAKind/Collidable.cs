﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OneOfAKind
{
    /// <summary>
    /// Denotes that this sprite blocks the player.
    /// </summary>
    interface Collidable
    {
        /// <summary>
        /// Returns whether you are currently collidable.
        /// </summary>
        /// <returns></returns>
        bool Colliding();
    }
}
