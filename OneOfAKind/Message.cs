﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using SdlDotNet;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;

namespace OneOfAKind
{
    /// <summary>
    /// A text message.
    /// </summary>
    public class Message : TextSprite
    {
        bool started = false;
        float startPoint;
        const int fadeRate = 3;
        int level;
        bool fadeout = false;

        public Message(string text, SdlDotNet.Graphics.Font font, Color color, Point position, float startPoint, int width, int level) :
            base(text, font, color, false, position) {
            this.TextWidth = width;
            this.Transparent = true;
            this.AlphaBlending = true;
            this.Alpha = (byte)0;
            this.startPoint = startPoint;
            this.level = level;
        }

        public override void Update(SdlDotNet.Core.TickEventArgs args) {
            base.Update(args);

            if (started) {
                int alpha = 0;
                if (fadeout) {
                    alpha = this.Alpha - fadeRate;
                    this.Alpha = (byte)alpha;
                } else {
                    alpha = this.Alpha + fadeRate;
                }

                if (alpha > 255) {
                    alpha = 255;
                }
                if (alpha < 0) {
                    alpha = 0;
                }
                this.Alpha = (byte)alpha;
            } else {
                if (OneOfAKind.Player.realX >= startPoint && OneOfAKind.CurrentLevelIndex == level) {
                    started = true;
                }
            }
        }

        public void FadeOut() {
            fadeout = true;
        }
    }
}
