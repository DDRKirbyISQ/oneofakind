﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using SdlDotNet;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;
using SdlDotNet.Input;
using SdlDotNet.Audio;

namespace OneOfAKind
{
    /// <summary>
    /// class for the player character's reflection.
    /// </summary>
    public class Reflection : AnimatedSprite
    {
        int level;

        // whether we're currently receiving input
        bool active = false;

        // Acceleration per frame.
        const float playerGravity = 0.2f;

        // Movement in pixels per frame.
        const float moveSpeed = 2.0f;

        // Jump velocity.
        const float jumpVelocity = 4.25f;

        // Ticks per animation frame.
        const int animationRate = 5;

        // Distance forward to create cloud.
        const int cloudDistance = 12;

        // how much down to search for player.
        const int activateOffset = 12 * Constants.TileSize;

        // Whether this is the endGame reflection.
        bool endGame;

        float yVelocity = 0;
        int animationCounter = 0;
        bool facingRight = true;

        // Note: we must always sync these to the underlying Sprite X and Y.
        public float realX; // upper-left
        public float realY; // upper-left corner

        public Reflection(Point coords, int level, bool endGame) {
            this.endGame = endGame;
            this.level = level;

            realX = coords.X;
            realY = coords.Y;
            X = (int)realX;
            Y = (int)realY;

            SurfaceCollection tempSurf;

            tempSurf = new SurfaceCollection();
            tempSurf.Add("img/reflection.png", new Size(12, 20), 0);
            foreach (Surface surface in tempSurf) { surface.Convert(Video.Screen); }
            AnimationCollection idleLeftAnim = new AnimationCollection();
            idleLeftAnim.Add(tempSurf);

            tempSurf = new SurfaceCollection();
            tempSurf.Add("img/reflection.png", new Size(12, 20), 1);
            foreach (Surface surface in tempSurf) { surface.Convert(Video.Screen); }
            AnimationCollection walkLeftAnim = new AnimationCollection();
            walkLeftAnim.Add(tempSurf);

            tempSurf = new SurfaceCollection();
            tempSurf.Add("img/reflection.png", new Size(12, 20), 2);
            foreach (Surface surface in tempSurf) { surface.Convert(Video.Screen); }
            AnimationCollection jumpLeftAnim = new AnimationCollection();
            jumpLeftAnim.Add(tempSurf);

            tempSurf = new SurfaceCollection();
            tempSurf.Add("img/reflection.png", new Size(12, 20), 3);
            foreach (Surface surface in tempSurf) { surface.Convert(Video.Screen); }
            AnimationCollection idleRightAnim = new AnimationCollection();
            idleRightAnim.Add(tempSurf);

            tempSurf = new SurfaceCollection();
            tempSurf.Add("img/reflection.png", new Size(12, 20), 4);
            foreach (Surface surface in tempSurf) { surface.Convert(Video.Screen); }
            AnimationCollection walkRightAnim = new AnimationCollection();
            walkRightAnim.Add(tempSurf);

            tempSurf = new SurfaceCollection();
            tempSurf.Add("img/reflection.png", new Size(12, 20), 5);
            foreach (Surface surface in tempSurf) { surface.Convert(Video.Screen); }
            AnimationCollection jumpRightAnim = new AnimationCollection();
            jumpRightAnim.Add(tempSurf);

            Animations.Add("idleleft", idleLeftAnim);
            Animations.Add("walkleft", walkLeftAnim);
            Animations.Add("jumpleft", jumpLeftAnim);
            Animations.Add("idleright", idleRightAnim);
            Animations.Add("walkright", walkRightAnim);
            Animations.Add("jumpright", jumpRightAnim);

            CurrentAnimation = "idleright";
            this.Transparent = true;
            this.TransparentColor = Color.FromArgb(255, 0, 255);


            realX = coords.X;
            realY = coords.Y;
            X = (int)realX;
            Y = (int)realY;
            yVelocity = 0;
            facingRight = true;
            active = false;
            this.CurrentAnimation = "idleright";

        }

        bool IsOnGround() {
            Y++;
            bool result = CollidingWithSomething();
            Y--;
            return result;
        }

        const int CollisionCheckMargin = Constants.TileSize * 2;

        bool CollidingWithSomething() {
            foreach (Sprite sprite in OneOfAKind.Levels[level].Sprites) {
                if (sprite is Collidable && ((Collidable)sprite).Colliding()) {
                    if (Math.Abs(sprite.X - this.X) < CollisionCheckMargin &&
                        Math.Abs(sprite.Y - this.Y) < CollisionCheckMargin) {
                        if (sprite.IntersectsWith(this)) {
                            return true;
                        }
                    }
                }
            }
            return false;
            /*
            SpriteCollection intersecting = OneOfAKind.Levels[level].Sprites.IntersectsWith(this);
            foreach (Sprite sprite in intersecting) {
                if (sprite is Collidable && ((Collidable)sprite).Colliding()) {
                    return true;
                }
            }
            return false;*/
        }

        public void Activate() {
            if (level != OneOfAKind.CurrentLevelIndex || active) {
                return;
            }

            Rectangle myRect = this.Rectangle;
            Rectangle playerRect = OneOfAKind.Player.Rectangle;
            myRect.Y += activateOffset;

            if (myRect.IntersectsWith(playerRect)) {
                active = true;
            }

            myRect.Y -= activateOffset;
        }

        public override void Update(SdlDotNet.Core.TickEventArgs args) {
            base.Update(args);

            // See whether to activate.
            Activate();

            if (active) {
                // Input, move left and right.
                if (Keyboard.IsKeyPressed(Key.RightArrow) && !Keyboard.IsKeyPressed(Key.LeftArrow)) {
                    facingRight = true;
                    realX += moveSpeed;
                    X = (int)realX;
                    while (CollidingWithSomething()) {
                        realX -= 1;
                        X = (int)realX;
                    }
                    this.CurrentAnimation = "walkright";
                } else if (Keyboard.IsKeyPressed(Key.LeftArrow) && !Keyboard.IsKeyPressed(Key.RightArrow)) {
                    facingRight = false;
                    realX -= moveSpeed;
                    X = (int)realX;
                    while (CollidingWithSomething()) {
                        realX += 1;
                        X = (int)realX;
                    }
                    this.CurrentAnimation = "walkleft";
                } else {
                    if (facingRight) {
                        this.CurrentAnimation = "idleright";
                    } else {
                        this.CurrentAnimation = "idleleft";
                    }
                }
            }

            if (endGame) {
                if (Keyboard.IsKeyPressed(Key.RightArrow) && !Keyboard.IsKeyPressed(Key.LeftArrow)) {
                    facingRight = true;
                    this.CurrentAnimation = "idleright";
                } else if (Keyboard.IsKeyPressed(Key.LeftArrow) && !Keyboard.IsKeyPressed(Key.RightArrow)) {
                    facingRight = false;
                    this.CurrentAnimation = "idleleft";
                } else {
                    if (facingRight) {
                        this.CurrentAnimation = "idleright";
                    } else {
                        this.CurrentAnimation = "idleleft";
                    }
                }
            }

            // If we're not on the ground, we're jumping
            if (!IsOnGround()) {
                if (facingRight) {
                    CurrentAnimation = "jumpright";
                } else {
                    CurrentAnimation = "jumpleft";
                }
            }

            // Falling
            int oldY = Y;

            // try to fall down or float up
            yVelocity += playerGravity;
            realY += yVelocity;

            // go up or down until we're not colliding anymore.
            Y = (int)realY;
            bool collided = false;
            while (CollidingWithSomething()) {
                collided = true;
                bool goingUp = yVelocity < 0;
                if (goingUp) {
                    realY += 1;
                } else {
                    realY -= 1;
                }
                Y = (int)realY;
            }
            if (collided) {
                yVelocity = 0;
            }

            // Finally, update sprite so we actually draw at the right place.
            X = (int)realX;
            Y = (int)realY;

            //animate.
            animationCounter++;
            if (animationCounter >= animationRate) {
                this.Frame++;
                animationCounter = 0;
            }
        }

        static Sound magicSound = new Sound("sfx/magic.ogg");
        static Sound respawnSound = new Sound("sfx/respawn.ogg");
        static Sound jumpSound = new Sound("sfx/jump.ogg");
        static Sound stepSound = new Sound("sfx/step.ogg");

        /// <summary>
        /// Handles keyboard presses (not holds).
        /// </summary>
        /// <param name="args"></param>
        public override void Update(KeyboardEventArgs args) {
            base.Update(args);

                // Jumping
                if (args.Key == Key.UpArrow && active) {
                    if (IsOnGround()) {
                        yVelocity = -jumpVelocity;
                        if (SdlDotNet.Audio.Mixer.FindAvailableChannel() > -1) {
                            jumpSound.Volume = 8;
                            jumpSound.Play();
                        }
                    }
                }
                // Aloneizing
            else if (args.Key == Key.Space || args.Key == Key.Return) {
                Point location = this.Center;
                if (facingRight) {
                    location.X += cloudDistance;
                } else {
                    location.X -= cloudDistance;
                }
                AloneCloud cloud = new AloneCloud(location, OneOfAKind.CurrentLevel(), true);
                OneOfAKind.CurrentLevel().Sprites.Add(cloud);
            }

            // Respawning
            else if (args.Key == Key.Escape || args.Key == Key.R) {
                realX = OneOfAKind.Levels[level].ReflectionSpawn.X;
                realY = OneOfAKind.Levels[level].ReflectionSpawn.Y;
                X = (int)realX;
                Y = (int)realY;
                yVelocity = 0;
                facingRight = true;
                active = false;
                this.CurrentAnimation = "idleright";
            }
        }
    }
}