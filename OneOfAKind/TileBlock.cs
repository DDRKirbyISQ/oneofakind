﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SdlDotNet;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;

namespace OneOfAKind
{
    // A 1x1 basic level tile.
    public class TileBlock : Sprite, Collidable
    {
        /// <summary>
        /// Creates the tile at the given grid coordinates.
        /// </summary>
        /// <param name="x">x grid coord</param>
        /// <param name="y">y grid coord</param>
        public TileBlock(int x, int y) {
            this.X = x * Constants.TileSize;
            this.Y = y * Constants.TileSize;
            this.Surface = new Surface("img/tile_block.png");
            Surface.Convert(Video.Screen);
            this.Visible = false;
        }

        public bool Colliding() {
            return true;
        }
    }
}
