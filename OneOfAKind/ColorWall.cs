﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using SdlDotNet;
using SdlDotNet.Graphics;
using SdlDotNet.Graphics.Sprites;

namespace OneOfAKind
{
    /// <summary>
    /// A colored wall that is alone-izable.
    /// </summary>
    public class ColorWall : Sprite, Collidable, Disappearable
    {
        bool visible = true;
        string disappearKey;
        string visibleImage;
        string disappearImage;

        public ColorWall(Point location, string disappearKey, string image, string disappearImage) : base(image) {
            this.Position = location;
            this.disappearKey = disappearKey;
            this.visibleImage = image;
            this.disappearImage = disappearImage;
            this.Surface.Convert(Video.Screen);
        }

        public string DisappearKey() {
            return disappearKey;
        }

        public bool IsVisible() {
            return visible;
        }

        public void SetVisible(bool visible) {
            this.visible = visible;
            if (visible) {
                this.Surface = new Surface(visibleImage);
            } else {
                this.Surface = new Surface(disappearImage);
            }
        }

        public bool Colliding() {
            return visible;
        }
    }
}
