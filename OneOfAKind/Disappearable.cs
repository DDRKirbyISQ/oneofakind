﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OneOfAKind
{
    /// <summary>
    /// Used to group collections of disappearable objects.
    /// </summary>
    interface Disappearable
    {
        /// <summary>
        /// Returns the key for disappearing.  Matching objects will disappear.
        /// </summary>
        /// <returns></returns>
        string DisappearKey();

        /// <summary>
        /// True if this object is visible, false otherwise.
        /// </summary>
        /// <returns></returns>
        bool IsVisible();

        /// <summary>
        /// Sets whether this is visible or not.
        /// </summary>
        /// <param name="visible"></param>
        void SetVisible(bool visible);
    }
}
