﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OneOfAKind
{
    /// <summary>
    /// Marks that a sprite can be deleted.
    /// </summary>
    interface Deletable
    {
        /// <summary>
        /// Returns true if we should delete you.
        /// </summary>
        /// <returns></returns>
        bool ShouldDelete();

        /// <summary>
        /// Marks this sprite for deletion.
        /// </summary>
        void MarkForDeletion();
    }
}
