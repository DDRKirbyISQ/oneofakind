﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace OneOfAKind
{
    public static class Constants
    {
        public const double FPS = 60;

        public const int WindowWidth = 640;
        public const int WindowHeight = 480;

        // Size of logical playfield - gets scaled up.
        public const int PlayfieldWidth = 640;
        public const int PlayfieldHeight = 480;

        // Tile size in pixels.
        public const int TileSize = 20;

        public const string Version = "1.02";

        public const int ScreenFlashAlpha = 192;
        public const int ScreenFlashDuration = 15;

        public const int ScreenFadeDuration = 60;

        public const int Levels = 17;

        public static Point PlayerStartCoords = new Point(TileSize*8, TileSize*17);
    }
}